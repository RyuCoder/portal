from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from company.models import Jobs, Companies


class UserCreateForm(UserCreationForm):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.EmailField(required=True)


    def __init__(self, data, help_text, *args, **kwargs):
        super(UserCreateForm, self).__init__(data, *args, **kwargs)

        # disable autofuocus on thr username field
        self.fields['username'].widget.attrs.update({'autofocus': ''})

        # set autofocus on the below field of the form
        self.fields['first_name'].widget.attrs.update(
            {'autofocus': 'autofocus'})

        # mechanism to disble the always ON help_text on the UserCreationForm
        self.help_text = help_text

        # if help_text is false, hide the help_text
        if self.help_text is False:
            for fieldname in ['username', 'password1', 'password2']:
                self.fields[fieldname].help_text = None


    class Meta:
        model = User
        fields = ("first_name", "last_name", "email",
                  "username", "password1", "password2")


class LoginForm(forms.Form):
    username = forms.CharField(initial="Admin")
    password = forms.CharField(min_length=6, widget=forms.PasswordInput())


    def clean(self, *args, **kwargs):

        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        user = User.objects.filter(username=username).first()

        if not username:
            raise forms.ValidationError("You must enter your Username.")

        if not password:
            raise forms.ValidationError("You must enter your Password.")

        if not user:
            raise forms.ValidationError(
                "User with the Username '{}' does not exist.".format(username))

        if not user.check_password(password):
            raise forms.ValidationError(
                "Username and Password does not match.")

        if not user.is_active:
            raise forms.ValidationError(
                "User with username '{}' is no longer active.".format(username))

        return super(LoginForm, self).clean(*args, **kwargs)


class ChangePassForm(forms.Form):

    old_password = forms.CharField(
        label="Old Password", min_length=6, widget=forms.PasswordInput())
    new_password = forms.CharField(
        label="New Password", min_length=6, widget=forms.PasswordInput())
    re_new_password = forms.CharField(
        label="Re-type New Password", min_length=6, widget=forms.PasswordInput())


    def __init__(self, data, user, *args, **kwargs):
        super(ChangePassForm, self).__init__(data, *args, **kwargs)
        self.user = user


    def clean_old_password(self, *args, **kwargs):
        old_password = self.cleaned_data.get('old_password')

        if not old_password:
            raise forms.ValidationError("You must enter your old password.")

        valid = self.user.check_password(self.cleaned_data.get('old_password'))

        if not valid:
            raise forms.ValidationError(
                "The old password that you have entered is wrong.")

        return old_password


    def clean_new_password(self, *args, **kwargs):
        new_password = self.cleaned_data.get('new_password')

        if not new_password:
            raise forms.ValidationError("You must enter a new password.")

        return new_password


    def clean_re_new_password(self, *args, **kwargs):
        new_password = self.cleaned_data.get('new_password')
        re_new_password = self.cleaned_data.get('re_new_password')

        if not re_new_password:
            raise forms.ValidationError("You must Re-enter new password")

        if new_password != re_new_password:
            raise forms.ValidationError(
                "New passwords that you have entered do not match.")

        return re_new_password


class NewJobForm(forms.ModelForm):

    class Meta:
        model = Jobs
        fields = (
                    'job_id',
                    'job_title',
                    'job_location',
                    'job_type',
                    'job_salary',
                    'job_starting_date'
                 )
                
        exclude = ['job_date_posted']
        
        widgets = {
                    'job_id': forms.TextInput(attrs={'readonly': True}),
                    'job_starting_date': forms.TextInput(attrs={'readonly': True}),
                  }


class CompanyForm(forms.ModelForm):

    class Meta:
        model = Companies

        fields = [
                    "company_id",
                    "company_logo",
                    "company_name",
                    "company_address",
                    "company_email",
                    "company_contact"
                 ]

        exclude = []

        widgets = {
                      'company_id': forms.HiddenInput(),
                  }


class SearchForm(forms.Form):
    
    class Meta:
        model = Jobs
        fields = (
                    'job_title',
                    'job_location',
                    'job_type',
                    'job_salary'
                 )
