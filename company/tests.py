from django.test import TestCase
from django.test import Client
from django.urls import reverse

from company.utils import max_string_length, generate_id, user_directory_path
from company.models import Companies, User

from pprint import pprint as pp  # for debugging purposes


class TestGenerateId(TestCase):


    def setUp(self):
        self.test_id = generate_id()


    def test_length_lowercase(self):
        self.assertTrue(self.test_id.islower())
        self.assertTrue(len(self.test_id) == max_string_length)
        self.assertTrue(self.test_id.isalnum())


class TestDirectoryPath(TestCase):


    def setUp(self):
        # Creating dummy objects
        class Company():
            pk = 3

        class Instance():
            company_id = Company()
            
        self.instance = Instance()
        self.filename = "Dragon.jpg"


    def test_directory_path(self):
        result = user_directory_path(self.instance, self.filename)
        result = result.split("/")

        self.assertTrue(result[0] == "companies")
        self.assertTrue( int(result[1]) == self.instance.company_id.pk)
        self.assertTrue(result[2] == self.filename)


class TestIndexPageLoad(TestCase):


    def setUp(self):
        self.client = Client()
        self.response = self.client.get('/')


    def test_check_response(self):
        self.assertTemplateUsed(self.response, 'index.html')
        self.assertEqual(self.response.charset, "utf-8")
        self.assertEqual(self.response.status_code, 200)


class TestReverseUrls(TestCase):


    def setUp(self):
        self.client = Client()
        self.reverseUrls = ['index', 'login', 'register']
        # self.response = self.client.get(reverse('register'))
        self.response = {url:self.client.get(reverse(url)) for url in self.reverseUrls}
    

    def test_reverse_urls(self):

        for url in self.reverseUrls:
            self.assertEqual(self.response[url].status_code, 200)


# Tests to write for scenrios
# -- Pagination size is what it is supposed to
# -- actual pagination is less than or equal to above size
# -- context contains particular variable 
# -- context variable is between its limits 
# -- flasdata is set and active when it is supposed to be and inactive when not set 
