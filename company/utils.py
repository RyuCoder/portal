from django.utils.crypto import get_random_string

# constant defined, use this variable instead of actual value
max_string_length = 9


def generate_id():
    """ Generates and returns an ID field containing 9 alphanumeric digits, alphabets are lowercase. """
    id = get_random_string(max_string_length).lower()
    return id


def user_directory_path(instance, filename):
    """ Generates and returns a string containing the path where the files are to be uploaded.  """
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'companies/{0}/{1}'.format(instance.company_id.pk, filename)
