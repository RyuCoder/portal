# Receiver callbacks for the django signals


def post_delete_jobs(sender, instance, *args, **kwargs):
    # Candidates that save a particular job, will be notified if a job is deleted.
    pass


def post_save_jobs(sender, instance, *args, **kwargs):
    # Candidates that follow a particular company will be notified when a new job is posted.
    pass
