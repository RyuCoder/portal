from django.conf import settings
from django.db import models
from django.core.urlresolvers import reverse
from django.db.models.signals import post_delete, post_save

from company.receivers import post_delete_jobs, post_save_jobs
from company.utils import max_string_length, generate_id, user_directory_path
from company.managers import DefaultManager, SqliteManager


# Monkey patching User model
User = settings.AUTH_USER_MODEL


class Jobs(models.Model):

    job_type_choices = (
                        ('I', 'Internship'),
                        ('T', 'Temporary'),
                        ('C', 'Contract'),
                        ('P', 'Permanant'),
                        ('O', 'Other'),
                       )

    job_salary_choices = (
                            ('ASP', 'As per standard'),
                            ('0', '< 1,00,000'),
                            ('1', '1,00,000 to 2,99,999'),
                            ('3', '3,00,000 to 5,99,999'),
                            ('6', '6,00,000 to 9,99,999'),
                            ('10', '10,00,000 +'),
                          )

    job_id = models.CharField(max_length=max_string_length, 
                                verbose_name="Job Id", default=generate_id)
    job_company_id = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Posted by company")  # foreign key of company
    job_title = models.CharField(max_length=256, verbose_name="Job Title")
    job_location = models.CharField(max_length=256, verbose_name="Job Location")
    job_type = models.CharField(max_length=1, choices=job_type_choices, verbose_name="Job Type", default="I")
    job_salary = models.CharField(max_length=3, choices=job_salary_choices, verbose_name="Salary", default="ASP")
    job_starting_date = models.CharField(default="ASAP", max_length=256, verbose_name="Joining Date")
    job_date_posted = models.DateTimeField(auto_now_add=True, verbose_name="Date Posted")

    objects = DefaultManager()
    sqlite = SqliteManager()


    def __str__(self):
        return self.job_title


    def get_absolute_url(self):
        return reverse('jobDetail', kwargs={'pk': self.pk})


    class Meta:
        verbose_name = "Job"
        verbose_name_plural = "Jobs"
        ordering = ["-job_date_posted"]


class Companies(models.Model):
    company_id = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    company_logo = models.FileField(upload_to=user_directory_path, blank=True, null=True, verbose_name="Company Logo")
    company_name = models.CharField(max_length=256, verbose_name="Company Name")
    company_address = models.TextField(verbose_name="Company Address")
    company_email = models.CharField(max_length=256, verbose_name="Company Email Id")
    company_contact = models.IntegerField(verbose_name="Company Contact")

    objects = DefaultManager()
    sqlite = SqliteManager()

    def __str__(self):
        return self.company_name


    class Meta:
        verbose_name = "Company"
        verbose_name_plural = "Companies"
        permissions = (
                        ("FE_add_job", "Can add new jobs"),
                        ("FE_detail_job", "Can see the details of a job"),
                        ("FE_update_job", "Can change the jobs"),
                        ("FE_delete_jobs", "Can delete a job"),
                      )


post_save.connect(post_save_jobs, sender=Jobs)
post_delete.connect(post_delete_jobs, sender=Jobs)
