from django.shortcuts import render, redirect, get_object_or_404

from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib import messages

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse

from django.urls import reverse_lazy

from django.views.generic import (TemplateView, ListView, DetailView, FormView, 
                                    CreateView, UpdateView, DeleteView, RedirectView)

from company.models import Jobs, Companies
from company.mixins import LoginRequiredMixin, IsOwnerOrNoAccess
from company.forms import (UserCreateForm, LoginForm, ChangePassForm, 
                            NewJobForm, CompanyForm, SearchForm)

from django.contrib.auth.mixins import PermissionRequiredMixin

from django.http import HttpResponseRedirect


class IndexViewCBV(TemplateView):
    template_name = 'index.html'


class RegisterCBV(CreateView):
    model = User
    template_name = "company/register.html"
    form_class = UserCreateForm
    success_url = reverse_lazy('login')


    def get_form_kwargs(self):
        kwargs = super(RegisterCBV, self).get_form_kwargs()
        kwargs['data'] = self.request.POST or None
        kwargs['help_text'] = False
        return kwargs


class LoginCBV(FormView):
    template_name = "company/login.html"
    form_class = LoginForm


    def get_success_url(self):
        return reverse('dashboard')


    def get_form_kwargs(self):
        kwargs = super(LoginCBV, self).get_form_kwargs()
        kwargs['data'] = self.request.POST or None
        # kwargs['user'] = self.request.user
        return kwargs


    def form_valid(self, form):

        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")

        user = auth.authenticate(username=username, password=password)
        auth.login(self.request, user)

        if self.next:
            return redirect(self.next)
        else:
            return redirect(self.get_success_url())


    def post(self, request, *args, **kwargs):

        form = self.get_form()

        self.next = request.GET.get("next")

        if request.user.is_authenticated:

            if self.next:
                return redirect(self.next)
            else:
                return redirect('dashboard')        

        if form.is_valid():
            return self.form_valid(form)
        else: 
            return self.form_invalid(form)


class DashboardCBV(LoginRequiredMixin, TemplateView):
    template_name = "company/dashboard.html"


    def get_context_data(self, **kwargs):
        context = super(DashboardCBV, self).get_context_data(**kwargs)
               
        context['count'] = Jobs.objects.filter(job_company_id=self.request.user.pk).count()
        context['applicants'] = "00"
        context['shortlisted'] = "00"
        context['joining'] = "00"

        return context


class SearchCBV(LoginRequiredMixin, ListView):
    template_name = "company/search.html"
    model = Jobs
    paginate_by = 3
    context_object_name = 'jobs'


    def get_queryset(self):
        jobs = Jobs.objects.filter(job_company_id=self.request.user.pk)

        # get search parameters from the request if any
        job_title = self.request.GET.get("job_title", '')
        job_location = self.request.GET.get("job_location", '')

        # default value is set to "-" in front end and backend 
        # to enable allowing to send empty values with no default
        job_type = self.request.GET.get("job_type", '-')
        job_salary = self.request.GET.get("job_salary", '-')
        
        # filter the jobs posted by the current company according to the search parameters
        if job_title != "":
            jobs = jobs.filter(job_title__icontains=job_title)

        if job_location != "":
            jobs = jobs.filter(job_location__iexact=job_location)

        # using for loop here only to make sure that 
        # the choices options are in a centralised location i.e. in the Jobs model
        for job in Jobs.job_type_choices:
            if job_type == job[0]:
                jobs = jobs.filter(job_type__iexact=job_type)
                break

        for salary in Jobs.job_salary_choices:
            if job_salary == salary[0]:
                jobs = jobs.filter(job_salary__iexact=job_salary)
                break

        for job in jobs:
            job.job_type = job.get_job_type_display()
            job.job_salary = job.get_job_salary_display()

        return jobs


    def get_context_data(self, **kwargs):
        context = super(SearchCBV, self).get_context_data(**kwargs)
       
        # context['count'] = self.get_queryset.count()
        context['count'] = len(context["jobs"])

        return context


class NewJobCBV(LoginRequiredMixin, CreateView):
    template_name = 'company/job.html'
    form_class = NewJobForm
    success_url = reverse_lazy('newJob')

    def form_valid(self, form):
        form.instance.job_company_id = self.request.user
        self.object = form.save()
        messages.success(self.request, "You have successfully posted a new job.")
        return redirect(self.get_success_url())


class JobDetailCBV(LoginRequiredMixin, IsOwnerOrNoAccess, DetailView):
    model = Jobs
    template_name = 'company/jobDetail.html'
    context_object_name = "job"
    
    no_permission_redirect_url = "dashboard"
    permission_denied_message = "You don't have permission to view this job."
    owner_field = "job_company_id"

    def get_object(self, queryset=None):
        obj = super(JobDetailCBV, self).get_object(queryset=queryset)
        obj.job_type = obj.get_job_type_display()
        obj.job_salary = obj.get_job_salary_display()
        return obj


class JobEditCBV(LoginRequiredMixin, IsOwnerOrNoAccess, UpdateView):
    model = Jobs
    template_name = 'company/jobEdit.html'
    form_class = NewJobForm
    success_url = reverse_lazy('search')

    no_permission_redirect_url = "dashboard"
    no_permission_error_message = "You don't have permission to edit this job."
    owner_field = "job_company_id"


class JobDeleteCBV(LoginRequiredMixin, IsOwnerOrNoAccess, DeleteView):
    model = Jobs
    success_url = reverse_lazy('search')
    
    no_permission_redirect_url = "dashboard"
    no_permission_error_message = "You don't have permission to delete this job."
    owner_field = "job_company_id"


class ProfileCBV(LoginRequiredMixin, TemplateView):
    template_name = 'company/profile.html'
    model = Companies
    context_object_name = "profile"


    def get(self, request, *args, **kwargs):
        
        try: 
            profile = Companies.objects.get(pk=request.user.pk) 
        except ObjectDoesNotExist:
            return redirect('profileCreate')

        context = {}
        context['profile'] = profile.__dict__
        
        return render(request, self.template_name, context)


class ProfileCreateCBV(LoginRequiredMixin, CreateView):
    template_name = 'company/profileCreate.html'
    form_class = CompanyForm
    model = Companies


    def get_success_url(self, **kwargs):
        return reverse("profile", kwargs={'pk': self.request.user.pk})


    def get_form_kwargs(self, **kwargs):
        kwargs = super(ProfileCreateCBV, self).get_form_kwargs(**kwargs)
        kwargs['initial'] = {'company_id': self.request.user.pk}
        return kwargs


class ProfileDeleteCBV(LoginRequiredMixin, DeleteView):
    model = Companies
    success_url = reverse_lazy('profileCreate')


class SettingsCBV(LoginRequiredMixin, FormView):
    template_name = 'company/settings.html'
    form_class = ChangePassForm
    success_url = reverse_lazy("settings")


    def get_form_kwargs(self):
        kwargs = super(SettingsCBV, self).get_form_kwargs()
        kwargs['data'] = self.request.POST or None
        kwargs['user'] = self.request.user
        return kwargs


    def form_valid(self, form):

        try:
            user = self.request.user
            new_password = form.cleaned_data.get("new_password")
            user.set_password('{}'.format(new_password))
            user.save()

            # if we dont call this function user will be logged out after password is changed.
            auth.update_session_auth_hash(self.request, user)

            messages.success(self.request, "You have successfully changed your password.")

        except Exception:
            messages.error(self.request,
                           "Something went wrong, we could not change your password. Please try again.")

        return super(SettingsCBV, self).form_valid(form)


    def get(self, request, *args, **kwargs):
        context = super(SettingsCBV, self).get_context_data(**kwargs)
        context["form"] = self.get_form()
        return render(request, "company/settings.html", context)


class LogoutCBV(LoginRequiredMixin, RedirectView):


    def get_redirect_url(self, *args, **kwargs):

        try:
            auth.logout(self.request)
            messages.success(self.request, "You have successfully logged out.")
        except Exception:
            messages.error(self.request, 
                            "Something went wrong, we could not logout your session. Please try again.")

        return reverse("login")
