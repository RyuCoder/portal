from django.apps import AppConfig


class CompanyConfig(AppConfig):
    name = 'company'

    def ready(self):
		# Register any custom signal here, import the callback receivers first
        pass
