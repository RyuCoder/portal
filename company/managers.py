from django.db import models


class DefaultManager(models.Manager):
    """ This manager will be used for the default database"""

    # def get_queryset(self):
    #     return super(DefaultManager, self).get_queryset()
    
    pass


class SqliteManager(models.Manager):
    """ This manager can be used to the test database"""

    
    def get_queryset(self):
        return super(SqliteManager, self).get_queryset().using("sqlite")
