from django.conf.urls import url

from company import views


urlpatterns = [

    url(r'^register/$', views.RegisterCBV.as_view(), name='register'),
    
    url(r'^login/$', views.LoginCBV.as_view(), name='login'),
    url(r'^dashboard/$', views.DashboardCBV.as_view(), name='dashboard'),
    url(r'^search/$', views.SearchCBV.as_view(), name='search'),

    url(r'^job/new/$', views.NewJobCBV.as_view(), name='newJob'),
    url(r'^job/detail/(?P<pk>\d+)/$', views.JobDetailCBV.as_view(), name='jobDetail'),
    url(r'^job/edit/(?P<pk>\d+)/$', views.JobEditCBV.as_view(), name='jobEdit'),
    url(r'^job/delete/(?P<pk>\d+)/$', views.JobDeleteCBV.as_view(), name='jobDelete'),

    url(r'^profile/create/$', views.ProfileCreateCBV.as_view(), name='profileCreate'),
    url(r'^profile/(?P<pk>\d+)/$', views.ProfileCBV.as_view(), name='profile'),
    url(r'^profile/delete/(?P<pk>\d+)/$', views.ProfileDeleteCBV.as_view(), name='profileDelete'),

    url(r'^settings/$', views.SettingsCBV.as_view(), name='settings'),
    url(r'^logout/$', views.LogoutCBV.as_view(), name='logout'),

]


