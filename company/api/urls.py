from django.conf.urls import url

from company.api import views


urlpatterns = [
    url(r'^user/(?P<pk>\d+)/$', views.UserDetailAPIView.as_view(), name='api-user-detail'),

    url(r'^companies/create/$', views.CompanyCreateAPIView.as_view(),
        name='api-companies-create'),
    url(r'^companies/list/$', views.CompaniesListAPIView.as_view(),
        name='api-companies-list'),
    
    url(r'^companies/detail/(?P<pk>\d+)/$',
        views.CompaniesDetailAPIView.as_view(), 
        name='api-companies-detail'),
    
    url(r'^companies/update/(?P<pk>\d+)/$',
        views.CompaniesUpdateAPIView.as_view(), name='api-companies-list'),
    url(r'^companies/delete/(?P<pk>\d+)/$',
        views.CompaniesDeleteAPIView.as_view(), name='api-companies-list'),
    

    url(r'^jobs/detail/(?P<pk>\d+)/$',
        views.JobsDetailAPIView.as_view(), 
        name='api-jobs-detail'),

    url(r'^jobs/list/$', views.JobsListAPIView.as_view(), name='api-jobs-list'),

]
