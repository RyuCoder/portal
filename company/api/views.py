from django.contrib.auth.models import User
from rest_framework.generics import (CreateAPIView, ListAPIView,
									RetrieveAPIView, UpdateAPIView, DestroyAPIView)

from company.api.serializers import JobSerializer, CompanySerializer, UserSerializer
from company.models import Jobs, Companies


class CompanyCreateAPIView(CreateAPIView):
    queryset = Companies.objects.all()
    serializer_class = CompanySerializer

    def perform_create(self, serializer):
        serializer.save(company_id=self.request.user)


class CompaniesListAPIView(ListAPIView):
    queryset = Companies.objects.all()
    serializer_class = CompanySerializer


class CompaniesDetailAPIView(RetrieveAPIView):
    queryset = Companies.objects.all()
    serializer_class = CompanySerializer
    lookup_field = 'pk'
    # lookup_url_kwarg = 'company_name'


class CompaniesUpdateAPIView(UpdateAPIView):
    queryset = Companies.objects.all()
    serializer_class = CompanySerializer


class CompaniesDeleteAPIView(DestroyAPIView):
    queryset = Companies.objects.all()
    serializer_class = CompanySerializer


class JobsListAPIView(ListAPIView):
    queryset = Jobs.objects.all()
    serializer_class = JobSerializer


class JobsDetailAPIView(RetrieveAPIView):
    queryset = Jobs.objects.all()
    serializer_class = JobSerializer


class UserDetailAPIView(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
