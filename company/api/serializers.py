from django.contrib.auth.models import User
from rest_framework import serializers

from company.models import Jobs, Companies


class UserSerializer2(serializers.HyperlinkedModelSerializer):
    # jobs = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='api-companies:api-jobs-detail')
    love = serializers.HyperlinkedRelatedField(many=True, 
                                                read_only=True,  
                                                view_name='api-companies:api-jobs-detail')
    # serializers.CharField(default="Jobs", max_length=255)
    # jobs = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'is_superuser', 'love')
        # fields = ('url', 'id', 'username', 'is_superuser')


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'is_superuser')


class CompanySerializer(serializers.HyperlinkedModelSerializer):
    # company_id = serializers.HyperlinkedRelatedField(lookup_field="pk", lookup_url_kwarg="pk",
                                # read_only=True, view_name='api-user-detail')
    # company_id = UserSerializer()
    company_id = serializers.SerializerMethodField()

    class Meta:
        model = Companies
        fields = ["company_id", "company_logo", "company_name",
                  "company_address", "company_email", "company_contact"]

    def get_company_id(self, obj):
        return str(obj.company_name)


class JobSerializer(serializers.ModelSerializer):
    # job_company_id = UserSerializer2()
    job_company_id = serializers.HyperlinkedRelatedField(
        read_only=True, view_name='api-companies:api-user-detail')
    
    class Meta:
        model = Jobs
        fields = ["id", "job_id", "job_company_id", "job_title", 
                    "job_location", "job_type", "job_salary", 
                    "job_starting_date", "job_date_posted"]
