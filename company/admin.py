from django.contrib import admin
from company.models import Jobs, Companies


class JobsAdmin(admin.ModelAdmin):

    search_fields = ['job_id', 'job_title', 'job_location', 'job_type']

    #  For job list view
    list_display = ['job_id', 'job_title', 'job_location', 'job_type',
                    'job_salary', 'job_starting_date', 'job_date_posted', 'job_company_id']
    
    #  For job detail view
    fields = ['job_id', 'job_title', 'job_location', 'job_type',
              'job_salary', 'job_starting_date', 'job_date_posted', 'job_company_id']
    
    # Admin should not be able to modify these fields
    readonly_fields = ['job_id', 'job_starting_date', 'job_date_posted']
    

    class Meta:
        model = Jobs


class CompaniesAdmin(admin.ModelAdmin):
    list_display = ["company_name", "company_address",
                    "company_email", "company_contact"]


    class Meta:
        model = Companies
        exclude = ["company_logo"]


admin.site.register(Jobs, JobsAdmin)
admin.site.register(Companies, CompaniesAdmin)
