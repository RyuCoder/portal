from pprint import pprint as pp

from django.core.exceptions import ImproperlyConfigured

from django.utils.decorators import method_decorator

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib import messages
from django.shortcuts import redirect

from django.conf import settings

from company.models import Jobs


class LoginRequiredMixin(object):
    """ This mixin makes sure that the user is logged in before accessing certain web pages. """

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class IsOwnerOrNoAccess(PermissionRequiredMixin):
    """ This mixin makes sure that the user has access to jobs posted only by him/her. """

    no_permission_redirect_url = ""
    owner_field = None

    def has_permission(self):
        
        owner_field = getattr(self.get_object(), self.owner_field, None)
        
        if owner_field is not None:
            if self.request.user == owner_field:
                return True
            else:
                return False
        else:
            raise ImproperlyConfigured("You must define owner_field attribute in the class.")


    def handle_no_permission(self):
        self._set_error_message()
        return self._perform_redirection()


    def _set_error_message(self):
        if self.permission_denied_message:
            messages.error(self.request, self.permission_denied_message)
        else:
            messages.error(self.request, "You don't have permission to access previous url.")


    def _perform_redirection(self):

        if self.no_permission_redirect_url:
            return redirect(self.no_permission_redirect_url)
        elif self.login_url:
            return redirect(self.login_url)
        elif settings.LOGIN_URL:
            return redirect(settings.LOGIN_URL)
        else:
            raise ImproperlyConfigured(
                "User has no permission to perform the previous action. \
                You must define no_permission_redirect_url attribute in the class or \
                define login_url attribute in the class or \
                define LOGIN_URL in the settings.py file of the project.")
